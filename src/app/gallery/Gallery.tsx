import { useState, useEffect} from 'react';
import NavBar from '../../components/NavbarNasa';
import Footer from '../../components/FooterNasa';
import FilterSort from '../../components/FilterSort';
import axios from 'axios';

// import ExploreSearchBar from './components/SearchBar';
// import Button from 'react-bootstrap/Button';
// import Modal from 'react-bootstrap/Modal';

// Today's Date
const date = new Date();
const todaysDate = new Date(date).toISOString().split('T')[0]

console.log("TODAYS DATE: " + todaysDate)
/**
 * NASA API: Astronomy Picture of the Day
 * Query key: values;
 * start_date: string; // can't be used with date
 * end_date: string; // The end of the date range, when used with start_date.
 * count: number; // If specified then count randomly chosen images will be returned. Cannot be used with date or start_date and end_date.
 */

interface Image {
    date: string;
    copyright: string;
    title: string;
    hdurl: string;
    explanation: string;
}

export default function GalleryView() {


    const apodUrl = "https://api.nasa.gov/planetary/apod?api_key=";
    const api_key = "EGIqmowv6z64cW991lB0IHfcUzHL656lBJ0ooV2A";


    const [imagesURL, setImagesUrl] = useState<Image[]>([]);

    // For Astrology Picture of the Day (APOD)API

        useEffect( () => {
            axios.get(apodUrl+api_key+"&start_date=2024-04-02").then( (response) => {
                //const tempImages : string[] = []
                //if (response.data) {
                    // console.log(response.data);
                    //response.data.data.map((image : Image) => {
                    //tempImages.push(....)
                //})
                setImagesUrl(response.data);
                
                }
                
            //}
            );
        }, []);

        // Modal Detailed View
        // const [show, setShow] = useState(false);
        // const handleClose = () => setShow(false);
        // const handleShow = () => setShow(true);

return (
    <div>
        <NavBar />
        <div id="gallery-page">
            <h1>Gallery</h1>

            <FilterSort />
            <br/><br/>
            
            {imagesURL.length
                ? imagesURL.map(
                    ( image , count) => {
                        return (
                        <div id="api-gallery">
                            <button id="details-button">
                                <img className="api-img" src={image.hdurl} alt="astronomy photograph of the day"/> 
                                <div id="detailed-view" className="mod-">
                                        {/*<div className="modal-cont-">*/}
                                        
                                    <p>Detailed View</p>
                                    <p> Title: {image.title}</p>
                                    <p> Date: {image.date}</p>
                                    <div id="picture-box">
                                        <button className="arrow-buttons"> &#8612; </button>
                                        <img className="detailed-img" src={image.hdurl} alt="astronomy photograph of the day"/> 
                                        <button className="arrow-buttons"> &#8614;</button>
                                    </div>
                                    
                                    <p id="explanation"> 
                                    Copyright: {image.copyright} <br/><br/>
                                    Explanation: {image.explanation}
                                    </p>
            
                                    
                                    {/*</div>*/}
                                </div>
                                </button>

                            {/*
                            <Modal show={show} onHide={handleClose} className="modal">
                                <div className="modal-content">
                                <Modal.Header closeButton>
                                    <Modal.Title><h1>{image.title}</h1></Modal.Title>
                                    <Modal.Title><h2>Date:{image.date}</h2></Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <img className="detailed-img" src={image.hdurl} alt="astronomy photograph of the day"/>
                                    <p>{image.explanation}</p>
                                    </Modal.Body>
                                <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>Close</Button>
                                </Modal.Footer>
                                </div>
                            </Modal>
                            */}


                        </div>
                        
                    )
                    }
                    ) : <></>
            }

            <Footer />
        </div>

        
    </div>
    )
}




/* -------------------------------------------------------------------
RENDER LISTS
function List(props) {
    const { numbers } = props;
    
    return (
    <ul>
        {numbers.map((number) =>
        <Item key={number.toString()} value={number} />
        )}
    </ul>
    );
}

function Item(props) {
    return <li>{props.value}</li>;
}

function DetailedView( props:Image) {
        const img = props;
    return (
            <div>   
                <h2>Detailed</h2>
                <h3>Title:{img.title}</h3>
                <p>Explanation: {img.explanation}</p>
            </div>
    
    );
}
*/