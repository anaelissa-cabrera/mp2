// Page Components
import './App.css';
import NavBar from '../components/NavbarNasa';
import Footer from '../components/FooterNasa';
import FilterSort from '../components/FilterSort';

// Media 
import nothing from '../resources/nothing-found.jpeg';


export default function NoMatch() {
    return (
        <div>
            <NavBar />
            <div className="gallery-page">
                <h1>Gallery</h1>

                <FilterSort />
                <br/><br/>

                <h2>No Results Found</h2>
                <img src={nothing} id="nothing" alt="nothing found"/>
                
                <Footer />
            </div>
    
            
        </div>
        )
    }