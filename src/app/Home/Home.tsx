import '../App.css';
// import axios from 'axios';
import NavBar from '../../components/NavbarNasa';
import Footer from '../../components/FooterNasa';
import {Link} from 'react-router-dom';

export default function HomeView() {

    const apodUrl = "https://api.nasa.gov/planetary/apod?api_key=";
    const api_key = "EGIqmowv6z64cW991lB0IHfcUzHL656lBJ0ooV2A";

    var req = new XMLHttpRequest();
    req.open("GET", apodUrl + api_key);
    req.send();

    // For Astrology Picture of the Day (APOD)API

    req.addEventListener("load", function(){
        if(req.status === 200 && req.readyState === 4){
        var response = JSON.parse(req.responseText);
        document.getElementById("title")!.textContent = response.title;
        document.getElementById("date")!.textContent = response.date;
        document.getElementById("pic")!.setAttribute("src",  response.hdurl);
        document.getElementById("explain")!.textContent = response.explanation;
        }
    })

    return (
        <div>
            <NavBar />
            <div className="main-page" id="nasa-page">
                <div>
                    <h1>Astronomy Picture of the Day</h1>

                    <p id="date"></p>
                    <p id="title"></p>
                    <p id="explain"></p>
                    <br/>
                    <button id="learn-button"><Link to="/gallery">Discover the Cosmos!</Link></button>
                    <br/><br/>
                    <div>
                    <img id="pic" src="" alt="today's astronomy of the day" />
                    </div>

                    
                </div>
            </div>
        <Footer />
            

        </div>
    )
}