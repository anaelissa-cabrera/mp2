import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import HomeView from './Home/Home';
import GalleryView from './gallery/Gallery';
import NoMatch from './NoMatch';

function App()  {

  return (
    <Router>
      <div>
      <Routes>
            <Route index element={<HomeView />}/>
              <Route path="/home" element={<HomeView />} />
              <Route path="/gallery" element={<GalleryView />}/>
              <Route path="/*" element={<HomeView />} />
              {/*<Route path="/*" element={<NoMatch />} />*/}
      </Routes>
      </div>
    </Router>
  );
}


export default App;
