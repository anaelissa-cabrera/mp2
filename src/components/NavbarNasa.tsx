import { Link } from 'react-router-dom';
import Explore from './SearchBar';
import logo from '../resources/nasa-logo.png'

export default function NavBar() {
    return (
        <nav>
            <div className='explore'> 
            <div> &#8205; &#8205; &#8205;</div>
            <div><Explore /></div>
            </div>

            <div className='logo'>
            <a href="/"><img src={logo} id="nasa-logo" alt="NASA logo"/></a>
            </div>

            <div className='links' id="nasa-links">
            <Link to="/home">Home &#8205; &#8205; &#8205; &#8205;</Link>
            <Link to="/gallery">Gallery  &#8205; &#8205; &#8205; &#8205;</Link>
            <a href="https://anaelissa-cabrera.gitlab.io/portfolio/"> &#8617; Portfolio</a>
            
            </div>
        </nav>

)
}