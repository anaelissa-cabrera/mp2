import React from 'react';
// import NoMatch from '../NoMatch';

// const query = "https://api.nasa.gov/planetary/apod?api_key=EGIqmowv6z64cW991lB0IHfcUzHL656lBJ0ooV2A&start_date=";

export default class ExploreSearchBar extends React.Component<{}, { value: string }>  {
constructor(props:any) {
    super(props);
    this.state = {
    value: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleClear  = this.handleClear.bind(this);
}

handleChange(event:any) {
    console.log("handleChange called! ")
    this.setState({value: event.target.value});
}

handleSubmit(event:any) {
    console.log("handleSubmit is called! ")
    alert(this.state.value);
    
    event.preventDefault();
}

handleSearch(event:any) {
    console.log("handleSubmit is called! ")
    alert(this.state.value);
    event.preventDefault();
}


handleClear(event:any) {
    console.log("handleClear is called! ")
    this.setState({value: ''});
    event.preventDefault();
}

render() {
    return (
    <form onSubmit={this.handleSubmit}>
        <label>
        Explore &#8205; &#8205; &#8205;
            <input
                id ="explore-input"
                type="search" 
                placeholder="Search..."
                value={this.state.value} 
                onChange={this.handleChange}

            />
        </label>
        <input id="search-button"  type="submit" hidden />
        <button id="clear-button" onClick={this.handleClear}>Clear</button>
        <div>
    
        </div>
    </form>

    );
}
}

