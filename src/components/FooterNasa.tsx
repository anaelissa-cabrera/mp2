export default function Footer() {
    return(
        <div>
            <div className="footer">
                    <div>
                    <br/> Ana Elissa Cabrera <br/>
                    CS 409: Web Programming <br/>
                    Machine Problem 2:  Front-end App <br />
                    Email : aec4@illinois.edu <br/>
                    GitLab Repo: https://gitlab.com/anaelissa-cabrera/portfolio <br/>
                    Website Link: https://anaelissa-cabrera.gitlab.io/portfolio <br/>
                    Inspiration: https://apod.nasa.gov/apod/astropix.html <br/>
                </div>

                <div className="footer-block">
                    <p>Let's Connect!</p>
            
                    <div className="icons">
                    <a href="https://www.linkedin.com/in/aecabreraleon/">
                        <img className="socials-button" alt="link to my linkedIn account"
                        src="https://static.vecteezy.com/system/resources/previews/018/930/587/original/linkedin-logo-linkedin-icon-transparent-free-png.png"/>
                    </a>
                
                    <a href="https://www.youtube.com/channel/UCe75mYHGoW3aMtvm7BELXCw/videos">
                        <img className="socials-button" alt="link to my youtube account"
                        src="https://static.vecteezy.com/system/resources/previews/018/930/572/original/youtube-logo-youtube-icon-transparent-free-png.png"/>
                    </a>

                    <a href="https://gitlab.com/anaelissa-cabrera">
                        <img className="socials-button" alt="link to my gitlab account"
                        src="https://pbs.twimg.com/profile_images/1526219998741647362/8KKDKESj_400x400.jpg"/>
                    </a>
                    
                    <a href="https://github.com/anaelissa-cabrera">
                        <img className="socials-button" alt="link to my github account"
                        src="https://www.pngmart.com/files/23/Github-Logo-PNG.png"/>
                    </a>
                    </div>
                </div>

                    <div>
                    <br/> NASA Sources <br/>
                    Authors & editors: Robert Nemiroff (MTU) & Jerry Bonnell (UMCP) <br/>
                    NASA Official: Amber Straughn Specific rights apply. <br/>
                    NASA Web Privacy, Accessibility Notices <br/>
                    A service of: ASD at NASA / GSFC, <br/>
                    NASA Science Activation <br/>
                    & Michigan Tech. U. <br/>
                    </div>


                </div>
        </div>
    )
}
